#!/bin/bash
# see https://freedumbytes.bitbucket.io/dpl/html/continuous.integration.pipeline.html#continuous.integration.pipeline.gitlab.touch.class
#Prevent unnecessary recompiling by maven-compiler-plugin when “Compiling xx source files” instead of “Nothing to compile - all classes are up to date” due to git clone action in the test job causing the .java file being more recent than the  corresponding .class file
find . -name "*.class" -exec touch {} \+