#!/bin/bash
# the GPG private keyshould be set in the env variable GPG_PRIVATE_KEY
# see https://www.gnupg.org/documentation/manuals/gnupg/Invoking-GPG_002dAGENT.html
export GPG_TTY=$(tty)
# add the GPG private key that should be define
gpg --batch --import <(echo "$GPG_PRIVATE_KEY")