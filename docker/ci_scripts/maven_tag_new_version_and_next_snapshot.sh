#!/bin/bash
# Steps:
# 1- Move the current project to the next release version
# 2- tag this version ( the build of the release should be done in a job launch on tag)
# 3- Move the current project to the next SNAPSHOT version
# 4- Commit
#
# Need a maven docker and that following variables.
#
# these variables are defined for the group fudaa
# + FUDAA_GIT_MAIL the mail for the user fudaa
# + FUDAA_GIT_USER the name of the user fudaa
# + GIT_SSH_PRIV_KEY the user's private key 
# 
# This variable is set in the pipeline
# + $MAVEN_CLI_OPTS options for maven
#
# This variable is set by gitlab:
# + CI_REPOSITORY_URL url for git

# SSH Initialization
# for explanation see https://docs.gitlab.com/ee/ci/ssh_keys/ 
eval $(ssh-agent -s)
echo "$GIT_SSH_PRIV_KEY" | tr -d '\r' | ssh-add - > /dev/null

mkdir -p ~/.ssh
chmod 700 ~/.ssh
ssh-keyscan gitlab.com  > ~/.ssh/known_hosts
chmod 644 ~/.ssh/known_hosts

#GIT
git config --global user.email $FUDAA_GIT_MAIL
git config --global user.name $FUDAA_GIT_USER
# to be able to git push: https://nicolaw.uk/GitLabCiPushingWrites
git remote set-url --push origin $(perl -pe 's#.*@(.+?(\:\d+)?)/#git@\1:#' <<< $CI_REPOSITORY_URL)


#Maven tag the release version
mvn $MAVEN_CLI_OPTS versions:set -DremoveSnapshot=true -DgenerateBackupPoms=false
CURRENT_PROJECT_VERSION=$(mvn -q -Dexec.executable=echo -Dexec.args='${project.version}' --non-recursive exec:exec)
git commit -a -m "release $CURRENT_PROJECT_VERSION"
git tag "release-$CURRENT_PROJECT_VERSION"
git push origin "release-$CURRENT_PROJECT_VERSION"
#Maven move to the next version
git checkout $CI_COMMIT_REF_NAME
mvn $MAVEN_CLI_OPTS versions:set -DnextSnapshot=true -DgenerateBackupPoms=false
CURRENT_PROJECT_VERSION=$(mvn -q -Dexec.executable=echo -Dexec.args='${project.version}' --non-recursive exec:exec)
git commit -a -m "change version to new snapshot $CURRENT_PROJECT_VERSION"
git push origin $CI_COMMIT_REF_NAME