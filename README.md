Common script for CI/CD tasks on Fudaa projects. 

Un exemple avec le projet fudaa-test-gitlab: 

* https://gitlab.com/fudaa/fudaa-test-gitlab
* https://gitlab.com/fudaa/fudaa-test-gitlab/blob/master/.gitlab-ci.yml

Construit une image docker avec des scripts et wine (utilisé par Fudaa-Crue pour modifier les icones des executables windows).
Le DockerFile est dans le répertoire docker.

Le scripts qui sont installées sous /ci_scripts/ dans le docker:
* https://gitlab.com/fudaa/fudaa-scripts/blob/master/docker/ci_scripts/install_gpg.sh permet d'initialiser la clé GPG du user fudaa
* https://gitlab.com/fudaa/fudaa-scripts/blob/master/docker/ci_scripts/prevent_maven_recompile.sh touch des .class pour éviter de recompiler inutilement entre les stages
* https://gitlab.com/fudaa/fudaa-scripts/blob/master/docker/ci_scripts/maven_tag_new_version_and_next_snapshot.sh incrémente les versions des fichiers pom (release puis SNAPSHOT et tague le tout)

Documentations annexes:
 * Les variables d'environnements utiles: http://doc.gitlab.com/ce/ci/variables/README.html